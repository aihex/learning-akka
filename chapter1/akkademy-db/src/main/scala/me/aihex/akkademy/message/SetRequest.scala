package me.aihex.akkademy.message

/**
  * Created by aihe on 7/29/16.
  */
case class SetRequest(key: String, value: Any)
