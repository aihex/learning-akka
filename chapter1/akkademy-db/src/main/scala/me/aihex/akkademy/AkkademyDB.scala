package me.aihex.akkademy

import akka.actor.Actor
import akka.event.Logging
import me.aihex.akkademy.message.SetRequest
import scala.collection.mutable

class AkkademyDB extends Actor {

  val map = new mutable.HashMap[String, Any].empty
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case SetRequest(key, value) => {
      log.info(s"received SetRequest - key: $key value: $value")
      map.put(key, value)
    }
    case o => log.info(s"received unknown message $o")
  }
}