package me.aihex.akkademy

import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import me.aihex.akkademy.message.SetRequest
import org.scalatest.{BeforeAndAfterEach, FunSpecLike, Matchers}

/**
  * Created by aihe on 7/29/16.
  */
class AkkademyDBSpec extends FunSpecLike with Matchers with BeforeAndAfterEach {
  implicit val system = ActorSystem()

  describe("AkkademyDB") {
    describe("given SetRequest") {
      it("should place key/value into map") {
        val actorRef = TestActorRef(new AkkademyDB())
        actorRef ! SetRequest("key", "value")
        val akkademyDB = actorRef.underlyingActor
        akkademyDB.map.get("key") should equal(Some("value"))
      }
    }
  }
}
