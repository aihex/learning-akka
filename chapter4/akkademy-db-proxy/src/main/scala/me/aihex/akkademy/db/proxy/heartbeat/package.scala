package me.aihex.akkademy.db.proxy

/**
  * Created by ahe on 8/6/16.
  */
package object heartbeat {

  case object Heartbeat

  case object RegisterHeartbeat

  case object ExpireDeadDB

}
