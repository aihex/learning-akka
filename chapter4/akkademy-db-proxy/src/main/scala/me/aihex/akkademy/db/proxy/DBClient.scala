package me.aihex.akkademy.db.proxy

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import me.aihex.akkademy.db.{DeleteRequest, GetRequest, SetIfNotExist, SetRequest}

import scala.concurrent.Future

/**
  * Created by ahe on 8/4/16.
  */
class DBClient(remoteAddress: String)(implicit val system: ActorSystem, implicit val timeout: Timeout) {


  private val remoteDB = system.actorSelection(remoteAddress)

  def set(key: String, value: Any): Future[_] = {
    ask(remoteDB, SetRequest(key, value))
  }

  def get(key: String): Future[_] = {
    ask(remoteDB, GetRequest(key))
  }

  def delete(key: String): Future[_] = {
    ask(remoteDB, DeleteRequest(key))
  }

  def setIfNotExist(key: String, value: Any): Future[_] = {
    ask(remoteDB, SetIfNotExist(key, value))
  }


}
