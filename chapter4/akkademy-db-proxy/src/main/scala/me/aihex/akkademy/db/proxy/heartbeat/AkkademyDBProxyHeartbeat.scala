package me.aihex.akkademy.db.proxy.heartbeat

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Stash}
import akka.event.Logging
import akka.pattern.AskTimeoutException
import akka.util.Timeout
import me.aihex.akkademy.db.proxy.DBClient
import me.aihex.akkademy.db.{Connected, Disconnected, Request}

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by ahe on 8/6/16.
  */
class AkkademyDBProxyHeartbeat(dbAddress: String) extends Actor with Stash {

  val dbRef = context.actorSelection(dbAddress)

  implicit val executor = context.dispatcher

  override def preStart(): Unit = {
    super.preStart()
    context.actorOf(Props(new HeartbeatActor))
  }

  override def receive: Receive = offline

  def offline: Receive = {
    case Connected =>
      unstashAll()
      context.become(online)
    case r: Request =>
      stash()
  }

  def online: Receive = {
    case Disconnected =>
      stash()
      context.become(offline)
    case r: Request =>
      dbRef.forward(r)
  }

  class HeartbeatActor extends Actor {

    implicit val executor = context.dispatcher
    val logger = Logging(context.system, this)

    var timestampMap = Map.empty[ActorRef, Long]

    override def preStart(): Unit = {
      super.preStart()
      dbRef ! RegisterHeartbeat
    }

    def expireDeadDB(): Unit = {
      val now = System.currentTimeMillis()
      for ((actor, time) <- timestampMap) {
        if (now - time > 4000) {
          logger.warning(s"actor $actor is dead")
          context.parent ! Disconnected
        }
      }
    }

    def offline: Receive = {
      case Heartbeat =>
        val senderRef = sender()
        logger.info(s"receive first heartbeat from $senderRef at time ${System.currentTimeMillis()}")
        timestampMap += (senderRef -> System.currentTimeMillis())
        context.system.scheduler.schedule(Duration.Zero, 2 seconds, self, ExpireDeadDB)
        context.parent ! Connected
        context.become(online)
    }

    def online: Receive = {
      case Heartbeat =>
        val senderRef = sender()
        logger.info(s"receive heartbeat from $senderRef at time ${System.currentTimeMillis()}")
        timestampMap += (senderRef -> System.currentTimeMillis())
        println(timestampMap)
        context.parent ! Connected
      case ExpireDeadDB =>
        expireDeadDB()
    }

    override def receive: Receive = offline
  }

}


object AkkademyDBProxyHeartbeat {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("proxySystem")
    implicit val executor = system.dispatcher
    implicit val timeout = Timeout(2 seconds)

    val dbActor = system.actorOf(Props[AkkademyDBHeartbeat], "db")
    val proxyActor = system.actorOf(Props(new AkkademyDBProxyHeartbeat(dbActor.path.toString)), "proxy")
    val client = new DBClient(proxyActor.path.toString)

    println("wait for connection")
    Thread.sleep(4000)

    client.set("1", "1")
    println(Await.result(client.get("1").mapTo[String].recover {
      case a: AskTimeoutException => "timeout"
      case e: Throwable => e.getMessage
    }, Duration.Inf))

    Thread.sleep(4000)

    println(Await.result(client.get("1").mapTo[String].recover {
      case a: AskTimeoutException => "timeout"
      case e: Throwable => e.getMessage
    }, Duration.Inf))

    system.stop(dbActor)

    Thread.sleep(8000)

    system.terminate().foreach { _ =>
      println("Actor system was shut down")
    }
  }
}