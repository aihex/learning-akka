package me.aihex.akkademy.db.proxy.ping

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorSystem, Props, Stash}
import akka.event.Logging
import akka.pattern.{AskTimeoutException, ask}
import akka.util.Timeout
import me.aihex.akkademy.db._
import me.aihex.akkademy.db.proxy.DBClient

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Success

/**
  * Created by ahe on 8/3/16.
  */
class AkkademyDBProxyPing(dbAddress: String) extends Actor with Stash {

  val dbRef = context.actorSelection(dbAddress)

  val maxAttempts = 2

  implicit val executor = context.dispatcher

  override def preStart(): Unit = {
    super.preStart()
    context.actorOf(Props(new PingActor))
  }

  override def receive: Receive = offline

  def offline: Receive = {
    case Connected =>
      unstashAll()
      context.become(online)
    case r: Request =>
      stash()
  }

  def online: Receive = {
    case Disconnected =>
      stash()
      context.become(offline)
    case r: Request =>
      dbRef.forward(r)
  }

  class PingActor extends Actor {

    implicit val executor = context.dispatcher
    implicit val timeout = Timeout(2, TimeUnit.SECONDS)
    val logger = Logging(context.system, this)

    override def preStart(): Unit = {
      super.preStart()
      context.system.scheduler.schedule(Duration.Zero, 2 seconds)(retry(1))
    }

    def retry(attempt: Int): Unit = {
      def helper(attempt: Int): Future[_] = {
        logger.info(s"Retrying attempt $attempt")
        (dbRef ? Ping) recoverWith {
          case e: AskTimeoutException =>
            if (attempt < maxAttempts) {
              helper(attempt + 1)
            } else {
              logger.error("maxAttempts reached")
              scala.concurrent.Future.failed(Disconnected)
            }
        }
      }
      val future = helper(attempt)
      future.onComplete {
        case Success(Connected) =>
          context.parent ! Connected
        case scala.util.Failure(e) =>
          context.parent ! Disconnected
        case e =>
          logger.warning(s"unknown message $e")
      }
    }

    override def receive: Receive = {
      case _ =>
    }
  }

}

object AkkademyDBProxyPing {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("proxySystem")
    implicit val executor = system.dispatcher
    implicit val timeout = Timeout(2 seconds)

    val dbActor = system.actorOf(Props[AkkademyDB], "db")
    val proxyActor = system.actorOf(Props(new AkkademyDBProxyPing(dbActor.path.toString)), "proxy")
    val client = new DBClient(proxyActor.path.toString)

    println("wait for connection")
    Thread.sleep(4000)

    client.set("1", "1")
    println(Await.result(client.get("1").mapTo[String].recover {
      case a: AskTimeoutException => "timeout"
      case e: Throwable => e.getMessage
    }, Duration.Inf))

    system.stop(dbActor)
    Thread.sleep(4000)

    println(Await.result(client.get("1").mapTo[String].recover {
      case a: AskTimeoutException => "timeout"
      case e: Throwable => e.getMessage
    }, Duration.Inf))
  }
}
