package me.aihex.akkademy.db.proxy.heartbeat

import akka.actor.ActorRef
import me.aihex.akkademy.db.AkkademyDB

import scala.concurrent.duration.Duration

import scala.concurrent.duration._

/**
  * Created by ahe on 8/6/16.
  */
class AkkademyDBHeartbeat extends AkkademyDB {

  implicit val executor = context.dispatcher

  var hbs = Set.empty[ActorRef]

  override def preStart(): Unit = {
    super.preStart()
    context.system.scheduler.schedule(Duration.Zero, 2 seconds) {
      hbs.foreach(_ ! Heartbeat)
    }
  }

  def receiveHeartbeat: Receive = {
    case RegisterHeartbeat =>
      val senderRef = sender()
      logger.info(s"receive registration from ${senderRef.path}")
      hbs += senderRef
  }

  override def receive: Receive = {
    receiveHeartbeat.orElse(super.receive)
  }

  override def postStop(): Unit = {
    super.postStop()
    hbs = Set.empty[ActorRef]
  }
}
