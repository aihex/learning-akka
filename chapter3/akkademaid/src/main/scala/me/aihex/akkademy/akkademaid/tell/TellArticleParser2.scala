package me.aihex.akkademy.akkademaid.tell

import akka.actor.{Actor, ActorRef, ActorSystem, Props, ReceiveTimeout, Status}
import akka.event.Logging
import akka.pattern.ask
import akka.util.Timeout
import me.aihex.akkademy.akkademaid._
import me.aihex.akkademy.db.{AkkademyDB, GetRequest, KeyNotFoundException, SetRequest}

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by ahe on 8/3/16.
  */
class TellArticleParser2(cacheActorPath: String,
                         httpClientActorPath: String,
                         articleParserActorPath: String) extends Actor {

  implicit val executor = context.dispatcher

  val cacheActor = context.actorSelection(cacheActorPath)
  val httpClientActor = context.actorSelection(httpClientActorPath)
  val articleParserActor = context.actorSelection(articleParserActorPath)

  val log = Logging(context.system, this)


  override def receive: Receive = {
    case ParseArticle(url) =>
      val senderRef = sender()
      context.actorOf(Props(new ServiceActor(url, senderRef)))
  }

  class ServiceActor(url: String, senderRef: ActorRef) extends Actor {

    override def preStart(): Unit = {
      println(s"${getClass.getSimpleName} ${self.path.toString} start")
    }

    context.setReceiveTimeout(5 seconds)

    cacheActor ! GetRequest(url)

    override def receive: Receive = {
      case ReceiveTimeout =>
        log.warning("time out")
        stop()
      case HttpResponse(body) =>
        articleParserActor ! ParseHtmlArticle(url, body)
      case body: String =>
        senderRef ! body
        stop()
      case ArticleBody(urll, body) =>
        cacheActor ! SetRequest(urll, body)
        println(senderRef)
        senderRef ! body
        stop()
      case Status.Failure(KeyNotFoundException(_)) =>
        log.warning(s"cache miss")
        httpClientActor ! url
      case Status.Success =>
        log.info("set cache")
      case e =>
        log.error(e.toString)
    }

    def stop(): Unit = {
      context.stop(self)
    }

    override def postStop(): Unit = {
      println(s"${getClass.getSimpleName} ${self.path.toString} stop")
    }
  }

}

object TellArticleParser2 {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("tellSystem")
    val cacheActor = system.actorOf(Props[AkkademyDB], "cache")
    val httpActor = system.actorOf(Props[HttpClientActor], "http")
    val parsingActor = system.actorOf(Props[ParsingActor], "parsing")

    implicit val executor = system.dispatcher
    implicit val timeout = Timeout(10 seconds)

    val tellParser = system.actorOf(
      Props(new TellArticleParser2(cacheActor.path.toString, httpActor.path.
        toString, parsingActor.path.toString)), "tell")
    try {
      val f = ask(tellParser, ParseArticle("http://heai.me/")).mapTo[String]
      val ret = Await.result(f, Duration.Inf)
      println(ret.length)

      val f2 = ask(tellParser, ParseArticle("http://heai.me/")).mapTo[String]
      val ret2 = Await.result(f2, Duration.Inf)
      println(ret2.length)
    } catch {
      case e: Exception => println(e)
    } finally {
      system.terminate().foreach { _ =>
        println("Actor system was shut down")
      }
    }
  }
}
