package me.aihex.akkademy.akkademaid

import akka.actor.{Actor, Status}
import akka.event.Logging

/**
  * Created by aihe on 8/1/16.
  */
class ParsingActor extends Actor {

  val logger = Logging(context.system, this)

  override def receive: Receive = {
    case ParseHtmlArticle(url, htmlString) =>
      sender() ! ArticleBody(url, de.l3s.boilerpipe.extractors.
        ArticleExtractor.INSTANCE.getText(htmlString))
    case _ =>
      logger.warning("unknown message")
      sender() ! Status.Failure(new Exception("unknown message"))
  }
}
