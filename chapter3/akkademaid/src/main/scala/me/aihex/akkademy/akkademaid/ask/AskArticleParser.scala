package me.aihex.akkademy.akkademaid.ask

import akka.actor.{Actor, ActorSystem, Props, Status}
import akka.event.Logging
import akka.pattern.ask
import akka.util.Timeout
import me.aihex.akkademy.akkademaid.{HttpClientActor, ParseArticle, ParsingActor, _}
import me.aihex.akkademy.db.{AkkademyDB, GetRequest, SetRequest}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}


/**
  * Created by aihe on 7/30/16.
  */
class AskArticleParser(cacheActorPath: String,
                       httpClientActorPath: String,
                       articleParserActorPath: String,
                       implicit val timeout: Timeout) extends Actor {

  implicit val executor = context.dispatcher

  val cacheActor = context.actorSelection(cacheActorPath)
  val httpClientActor = context.actorSelection(httpClientActorPath)
  val articleParserActor = context.actorSelection(articleParserActorPath)


  val log = Logging(context.system, this)


  override def receive: Receive = {
    case ParseArticle(url) =>
      val senderRef = sender()
      val cacheResult = ask(cacheActor, GetRequest(url)).recoverWith {
        case _: Exception =>
          val rawResult = ask(httpClientActor, url)
          rawResult.flatMap {
            case HttpResponse(html) =>
              ask(articleParserActor, ParseHtmlArticle(url, html))
            case _ => Future.failed(new Exception("unknown exception"))
          }
      }

      cacheResult.onComplete {
        case scala.util.Success(x: String) =>
          log.info("cache hit")
          senderRef ! x
        case scala.util.Success(ArticleBody(_, body)) =>
          log.info("cache miss")
          cacheActor ! SetRequest(url, body)
          senderRef ! body
        case scala.util.Failure(ex) =>
          senderRef ! Status.Failure(ex)
        case _ => println("unknown message")
      }
  }
}

object AskArticleParser {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("askSystem")
    val cacheActor = system.actorOf(Props[AkkademyDB], "cache")
    val httpActor = system.actorOf(Props[HttpClientActor], "http")
    val parsingActor = system.actorOf(Props[ParsingActor], "parsing")

    implicit val executor = system.dispatcher
    implicit val timeout = Timeout(10 seconds)

    val askParser = system.actorOf(
      Props(classOf[AskArticleParser], cacheActor.path.toString, httpActor.path.
        toString, parsingActor.path.toString, timeout), "ask")
    val f = ask(askParser, ParseArticle("http://heai.me/")).mapTo[String]
    val ret = Await.result(f, 5 seconds)
    println(ret.length)

    val f2 = ask(askParser, ParseArticle("http://heai.me/")).mapTo[String]
    val ret2 = Await.result(f2, 5 seconds)
    println(ret2.length)

    system.terminate().foreach { _ =>
      println("Actor system was shut down")
    }
  }
}