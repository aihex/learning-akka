package me.aihex.akkademy.akkademaid

import akka.actor.{Actor, ActorRef, ReceiveTimeout, Status}
import akka.pattern.pipe
import com.ning.http.client.{AsyncCompletionHandler, AsyncHttpClient, Response}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future, Promise}

/**
  * Created by aihe on 8/1/16.
  */
class RetrievingActor(url: String, actor: ActorRef) extends Actor {

  implicit val executor = context.dispatcher

  val client = new AsyncClient

  context.setReceiveTimeout(5 seconds)

  override def preStart(): Unit = {
    println(s"${getClass.getSimpleName} ${self.path.toString} start")
    client.get(url).pipeTo(self)
  }

  override def receive: Receive = {
    case body: String =>
      actor ! HttpResponse(body)
      stop()
    case ReceiveTimeout =>
      stop()
    case e@Status.Failure(_) =>
      actor ! e
      stop()
  }

  def stop(): Unit = {
    context.stop(self)
  }

  override def postStop(): Unit = {
    println(s"${getClass.getSimpleName} ${self.path.toString} stop")
    client.shutdown()
  }

}

class AsyncClient {

  private val client = new AsyncHttpClient

  def get(url: String)(implicit exec: ExecutionContextExecutor): Future[String] = {
    val f = client.prepareGet(url)
    val p = Promise[String]()

    f.execute(new AsyncCompletionHandler[Unit] {
      def onCompleted(response: Response): Unit = {
        if (response.getStatusCode / 100 < 4)
          p.success(response.getResponseBody)
        else p.failure(BadStatus(response.getStatusCode))
      }

      override def onThrowable(t: Throwable): Unit = {
        p.failure(t)
      }
    })
    p.future
  }

  def shutdown(): Unit = client.close()

}
