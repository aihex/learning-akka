package me.aihex.akkademy

/**
  * Created by aihe on 7/30/16.
  */
package object akkademaid {

  case class ParseArticle(url: String)

  case class HttpResponse(body: String)

  case class ParseHtmlArticle(url: String, htmlString: String)

  case class ArticleBody(url: String, body: String)

  case class BadStatus(status: Int) extends RuntimeException

}
