package me.aihex.akkademy.akkademaid

import akka.actor.{Actor, ActorRef, Props}
import akka.event.Logging

/**
  * Created by aihe on 8/1/16.
  */
class HttpClientActor extends Actor {

  implicit val executor = context.dispatcher

  val logger = Logging(context.system, this)

  def props(url: String, actor: ActorRef): Props = Props(new RetrievingActor(url, actor))

  override def receive: Receive = {
    case url: String =>
      logger.info(s"receive $url")
      val senderRef = sender()
      context.actorOf(props(url, senderRef))
    case _ =>
      logger.error("unknown message")
  }
}
