package me.aihex.akkademy.db.client

import me.aihex.akkademy.db.KeyNotFoundException
import org.scalatest.{FunSpecLike, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by ahe on 7/29/16.
  */
class DBClientIntegrationSpec extends FunSpecLike with Matchers {

  val client = new DBClient("127.0.0.1:2552")

  describe("DBClinet") {
    it("should set a value") {
      client.set("123", 123)
      val f = client.get("123")
      val ret = Await.result(f, 10 seconds)
      ret should equal(123)
    }
    it("should fail when get unset value") {
      val f = client.get("1")
      intercept[KeyNotFoundException] {
        Await.result(f, 1 second)
      }
    }
    it("should not cover existing value") {
      client.setIfNotExist("123", 321)
      val f = client.get("123")
      val ret = Await.result(f, 10 seconds)
      ret should equal(123)
    }
    it("should delete a key") {
      client.delete("123")
      val f = client.get("123")
      intercept[KeyNotFoundException] {
        Await.result(f, 5 second)
      }
    }
  }

}
