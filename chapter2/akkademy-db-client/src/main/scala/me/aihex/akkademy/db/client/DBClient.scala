package me.aihex.akkademy.db.client

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import me.aihex.akkademy.db._

import scala.concurrent.Future
import scala.concurrent.duration._

class DBClient(remoteAddress: String) {

  private implicit val timeout = Timeout(2 seconds)
  private implicit val system = ActorSystem("ClientSystem")
  private val remoteDB = system.actorSelection(s"akka.tcp://DBSystem@$remoteAddress/user/akkademy-db")

  def set(key: String, value: Any): Future[_] = {
    ask(remoteDB, SetRequest(key, value))
  }

  def get(key: String): Future[_] = {
    ask(remoteDB, GetRequest(key))
  }

  def delete(key: String): Future[_] = {
    ask(remoteDB, DeleteRequest(key))
  }

  def setIfNotExist(key: String, value: Any): Future[_] = {
    ask(remoteDB, SetIfNotExist(key, value))
  }

}
