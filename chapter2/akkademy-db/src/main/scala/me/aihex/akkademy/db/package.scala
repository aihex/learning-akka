package me.aihex.akkademy

/**
  * Created by aihe on 7/30/16.
  */
package object db {

  trait Request

  case class GetRequest(key: String) extends Request

  case class SetRequest(key: String, value: Any) extends Request

  case class KeyNotFoundException(key: String) extends Exception

  case class KeyExistException(key: String) extends Exception

  case class SetIfNotExist(key: String, value: Any) extends Request

  case class DeleteRequest(key: String) extends Request

  case object Ping

  case object Connected

  case object Disconnected extends Exception

}
