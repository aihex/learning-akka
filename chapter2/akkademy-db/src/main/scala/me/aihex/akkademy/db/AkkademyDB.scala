package me.aihex.akkademy.db

import akka.actor.{Actor, ActorSystem, Props, Status}
import akka.event.Logging

import scala.collection.mutable

class AkkademyDB extends Actor {

  val map = new mutable.HashMap[String, Any].empty
  val logger = Logging(context.system, this)


  override def receive: Receive = {
    case SetRequest(key, value) =>
      logger.info(s"receive SetRequest - key:$key value: ${value.toString.take(10)}")
      map.put(key, value)
      sender() ! Status.Success
    case GetRequest(key) =>
      logger.info(s"received GetRequest - key: $key")
      map.get(key) match {
        case Some(value) => sender() ! value
        case None => sender() ! Status.Failure(KeyNotFoundException(key))
      }
    case SetIfNotExist(key, value) =>
      logger.info(s"received SetIfNotExist - key: $key value: ${value.toString.take(10)}")
      map.get(key) match {
        case None =>
          map.put(key, value)
          sender() ! Status.Success
        case _ =>
          sender() ! Status.Failure(KeyExistException(key))
      }
    case DeleteRequest(key) =>
      logger.info(s"received DeleteRequest - key: $key")
      map.get(key) match {
        case None =>
          sender() ! Status.Failure(KeyNotFoundException(key))
        case _ =>
          map -= key
          sender() ! Status.Success
      }
    case Ping =>
      sender() ! Connected
    case o => Status.Failure(new ClassNotFoundException())
  }
}

object AkkademyDB extends App {
  val system = ActorSystem("DBSystem")
  val actor = system.actorOf(Props[AkkademyDB], name = "akkademy-db")
}
